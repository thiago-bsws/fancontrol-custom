#!/bin/bash
coolerfile='/sys/class/hwmon/hwmon2/pwm1'
tempfile='/sys/class/hwmon/hwmon2/temp1_input'
while true
do
sleep 5
t="$(cat ${tempfile})"
v="$(cat ${coolerfile})"
echo "Current CPU temp is $t"
  if [ $t -gt 77000 ] 
  then
	if [ $v -ne 255 ] 
  	then
        echo 255 > $coolerfile
	fi
	continue
  fi
  if [ $t -gt 70000 ]
  then
	if [ $v -ne 225 ] 
  	then
        echo 225 > $coolerfile
	fi
	continue
  fi
  if [ $t -gt 60000 ]
  then
	if [ $v -ne 200 ] 
  	then
        echo 200 > $coolerfile
	fi
	continue
  fi
  if [ $t -gt 50000 ]
  then
	if [ $v -ne 175 ] 
  	then
        echo 175 > $coolerfile
	fi
	continue
  fi
done

